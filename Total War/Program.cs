﻿using System.Collections.Immutable;
using System.Text;
using ExhaustiveMatching;
using LanguageExt;
using Total_War;
using static LanguageExt.Prelude;


Eff<Unit> RunGame(Game game)
{
    return
        from nextRound in game.NextRound()
        let gameLoop = (
            from round in nextRound
            let printEff = round.PrintResult()
            let runGame = printEff.Bind(_ => round.IsEnded() ? Eff(() => Unit.Default) : RunGame(round))
            select runGame)
        from b in gameLoop.IfLeft(e => FailEff<Unit>(e))
        select b;
}

RunGame(new Game(new GameRound(0),
    new Player(Ownership.Player1, new Castle(new Gold(20), new HP(100)), new RandomBrain()),
    new Player(Ownership.Player2, new Castle(new Gold(20), new HP(100)), new RandomBrain()),
    new Option<GameUnit>[30].ToImmutableList()
    ))
.Run().Match(_ => { }, e =>
{
    Console.Error.WriteLine(e);
    Environment.Exit(1);
});

static class Extensions
{
    public static string Right(this string input, int length)
    {
        return input.Length < length ? input : input.Substring(input.Length - length, length);
    }

    public static string Repeat(this string input, uint repeat)
    {
        var stringBuilder = new StringBuilder();
        for (var i = 0; i < repeat; i++)
        {
            stringBuilder.Append(input);
        }
        return stringBuilder.ToString();
    }

    public static Eff<Unit> WriteLineToConsole(this StringBuilder stringBuilder)
    {
        return Eff(() =>
        {
            Console.WriteLine(stringBuilder.ToString());
            return Unit.Default;
        });
    }

    public static Eff<Unit> PrintResult(this Game game)
    {
        /*
         * 
    [Gold] P1: 15 / 100 | P2: 30 / 100

    ### . . . . ] . . . } . . ) ) . ] . ] . . [ . [ . { . ( . ( . ###
    100         6       4     4 6   8   8     9   9   7   5   5   100 <- HP indicators
      ^         ^       ^     ^ ^   ^   ^     ^   ^   ^   ^   ^   ^
       \         \_______\_____\_\___\___\     \___\___\___\___\   \__ P2 castle
        \__ Player 1 (P1) castle          \__ P1 Units          \__ P2 Units
         */
        var stringBuilder = new StringBuilder();
        stringBuilder.AppendLine("-".Repeat(70));
        stringBuilder.AppendLine($"Round {game.gameRound}: {(game.gameRound.Value % 2 == 1 ? "Player1" : "Player2")}");
        stringBuilder.AppendLine($"[Game] P1: {game.player1.Castle.Gold}/{Castle.MaxGold} | " +
            $"{game.player2.Castle.Gold}/{Castle.MaxGold}");
        stringBuilder.Append("### ");
        var lineWorld = new string[game.world.Count];
        var lineWorldLife = new string[game.world.Count];
        for (var i = 0; i < game.world.Count; i++)
        {

            lineWorld[i] = game.world[i].Map(x => x.Type switch
            {
                GameUnitType.Cleric => x.Owner == Ownership.Player1 ? ")" : "(",
                GameUnitType.Warrior => x.Owner == Ownership.Player1 ? "]" : "[",
                GameUnitType.Archer => x.Owner == Ownership.Player1 ? "}" : "{",
                _ => throw ExhaustiveMatch.Failed(x.Type)
            }).IfNone(() => ".");
            lineWorldLife[i] = game.world[i].Map(x => $"{x.Hp}").IfNone(() => " ");
        }
        stringBuilder.AppendJoin(" ", lineWorld);
        stringBuilder.AppendLine(" ###");
        stringBuilder.Append($"   {game.player1.Castle.Hp}".Right(3));
        stringBuilder.Append(' ');
        stringBuilder.AppendJoin(" ", lineWorldLife);
        stringBuilder.Append(' ');
        stringBuilder.AppendLine($"   {game.player2.Castle.Hp}".Right(3));
        return stringBuilder.WriteLineToConsole();
    }
}
