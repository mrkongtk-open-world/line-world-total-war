﻿using ExhaustiveMatching;
using LanguageExt;
using LanguageExt.ClassInstances;

namespace Total_War;

public class HP : NumType<HP, TInt, int>
{
    public HP(int value) : base(value) { }

    public static readonly HP ZERO_HP = new HP(0);
    public static readonly HP MAX_HP = new HP(int.MaxValue);
}

public class Gold : NumType<Gold, TInt, int>
{
    public Gold(int value) : base(value) { }
}

public class Damage : NumType<Damage, TInt, int>
{
    public Damage(int value) : base(value) { }
}

public class WorldRange : NumType<WorldRange, TInt, int>
{
    public WorldRange(int value) : base(value) { }
}

public class AttackRange : WorldRange
{
    public AttackRange(int value) : base(value) { }
}

public class Heal : NumType<Heal, TInt, int>
{
    public Heal(int value) : base(value) { }
}

public class HealRange : WorldRange
{
    public HealRange(int value) : base(value) { }
}

public class CoolDown : NumType<CoolDown, TInt, int>
{
    public CoolDown(int value) : base(value) { }
}

static class GameUnitPropertyExtensions
{
    /// <summary>
    /// Gets the direction for a player to move in a line world.
    /// Player 1 moves to the right and Player 2 moves to the left.
    /// </summary>
    /// <param name="owner">The player for which to get the direction</param>
    /// <returns>-1 for left, 1 for right</returns>
    public static int DirectionalStep(this Ownership owner)
    {
        return owner == Ownership.Player2 ? -1 : 1;
    }

    /// <summary>
    /// Retrieves the maximum hp of a specific type of game unit
    /// </summary>
    public static HP MaxHp(this GameUnitType unitType)
    {
        return unitType switch
        {
            GameUnitType.Cleric => new HP(3),
            GameUnitType.Archer => new HP(5),
            GameUnitType.Warrior => new HP(9),
            _ => throw ExhaustiveMatch.Failed(unitType)
        };
    }

    /// <summary>
    /// Retrieves the cost in gold for a brain action taken
    /// </summary>
    public static Gold Cost(this BrainAction brainAction)
    {
        return brainAction switch
        {
            BrainAction.SpawnCleric => new Gold(30),
            BrainAction.SpawnArcher => new Gold(20),
            BrainAction.SpawnWarrior => new Gold(15),
            _ => throw ExhaustiveMatch.Failed(brainAction)
        };
    }
}