﻿using LanguageExt;
using LanguageExt.Common;

namespace Total_War;

public enum GameUnitType
{
    Cleric,
    Warrior,
    Archer,
}

public record Castle(Gold Gold, HP Hp)
{
    public static readonly Gold MaxGold = new Gold(100);

    /// <summary>
    /// Generates gold for the castle each round, but not more than the maximum capacity.
    /// </summary>
    /// <returns>An updated castle with the new amount of gold.</returns>
    public Castle GenerateResource()
    {
        return this with { Gold = (this.Gold + new Gold(10)).Min(MaxGold) };
    }

    public Castle TakeDamage(Damage damage)
    {
        return this with { Hp = this.Hp - new HP(damage.Value) };
    }

    public Either<Error, Castle> UseGold(Gold gold)
    {
        return this.Gold >= gold ?
            this with { Gold = this.Gold - gold } :
            Error.New("not enough gold");
    }
}

public record GameUnit(GameUnitType Type, HP Hp, Damage Damage, AttackRange AttackRange,
    Heal Heal, CoolDown HealCoolDown, HealRange HealRange, Ownership Owner)
{
    public static readonly CoolDown MaxHealCoolDown = new CoolDown(3);

    public GameUnit TakeDamage(Damage damage)
    {
        return this with { Hp = (this.Hp - new HP(damage.Value)) };
    }

    /// <summary>
    /// Restores the game unit's HP, but does not exceed the unit's maximum HP.
    /// </summary>
    /// <returns>The updated game unit with a new HP value.</returns>
    public GameUnit TakeHeal(Heal heal)
    {
        return this with
        {
            Hp = (this.Hp + new HP(heal.Value)).Min(this.Type.MaxHp())
        };
    }
}