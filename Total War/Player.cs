﻿using System.Numerics;
using ExhaustiveMatching;
using LanguageExt;
using LanguageExt.Common;
using static LanguageExt.Prelude;

namespace Total_War;

public enum Ownership
{
    Player2,
    Player1,
}

public enum BrainAction
{
    SpawnWarrior,
    SpawnArcher,
    SpawnCleric,
}

/// <summary>
/// The interface of the Brain that determines the next action based on the current game state.
/// </summary>
public interface IBrain
{
    Eff<Option<BrainAction>> NextAction(Game game);
}

/// <summary>
/// A class that randomly generates the next action among all possible options in the game.
/// These options are determined by the remaining game world space and the player's available gold.
/// </summary>
public class RandomBrain : IBrain
{
    public Eff<Option<BrainAction>> NextAction(Game game)
    {
        if (game.world.Filter(x => x.IsNone).Length() <= 0)
        {
            return Eff(() => Option<BrainAction>.None);
        }
        var possibleActions = Enum.GetValues<BrainAction>().Filter(x =>
            x.Cost() <= game.GetPlayer(game.Owner).Castle.Gold
        );
        return possibleActions.GetRandom();
    }
}

/// <summary>
/// The Player record class stores information about a player in the game,
/// including their ownership type, Castle, and Brain.
/// </summary>
public record Player(Ownership OwnerType, Castle Castle, IBrain Brain)
{
    /// <summary>
    /// This method allows a player to use a specific amount of gold,
    /// updating their castle accordingly.
    /// </summary>
    /// <param name="gold">The specified amount of gold to be used</param>
    /// <returns>
    /// Right: An updated Player object with a modified Castle
    /// Left: The specified amount of gold to be used is greater than the player's current gold supply.
    /// </returns>
    public Either<Error, Player> UseGold(Gold gold)
    {
        return this.Castle.UseGold(gold).Map(x => this with { Castle = x });
    }
}

public static class Extensions
{
    /// <summary>
    /// Retrieves a random item from the provided iterator
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="iterator">an iterator of items</param>
    /// <returns>
    /// Some: any item inside the iterator
    /// None: if the iterator is empty
    /// </returns>
    public static Eff<Option<T>> GetRandom<T>(this IEnumerable<T> iterator)
    {
        return Eff(() => iterator
            .Map(x => Some(x))
            .ElementAtOrDefault(
                iterator.Count() == 0
                    ? -1
                    : (DateTime.Now.Millisecond * 7919) % iterator.Count())
            );
    }

    public static Ownership Opposite(this Ownership moveDirection)
    {
        return moveDirection switch
        {
            Ownership.Player2 => Ownership.Player1,
            Ownership.Player1 => Ownership.Player2,
            _ => throw ExhaustiveMatch.Failed(moveDirection)
        };
    }
}
