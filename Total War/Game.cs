﻿using System.Collections.Immutable;
using ExhaustiveMatching;
using LanguageExt;
using LanguageExt.ClassInstances;
using LanguageExt.Common;
using static LanguageExt.Prelude;

namespace Total_War;

public class GameRound : NumType<GameRound, TInt, int>
{
    public GameRound(int value) : base(value) { }
}

/// <summary>
/// Stores the game unit and gold cost needed to spawn it in the world.
/// </summary>
public record SpawnResource(GameUnit Unit, Gold Cost);
/// <summary>
/// Stores the game unit that is being prepared to spawn in the world,
/// along with the game state after the castle has spawned the game unit,
/// but the game unit is not yet in the world.
/// </summary>
public record SpawnPreparedResource(GameUnit Unit, Game Game);
/// <summary>
/// Stores the game unit and game state after the game unit has taken any action,
/// such as reducing healing cooldown, healing, attacking, or walking.
/// </summary>
public record UnitSingleActionResponse(GameUnit Unit, Game Game);
/// <summary>
/// Stores the index position and the game unit.
/// This information can be used to track the location of the game unit in the world,
/// as the index can be used to identify the game unit in that state,
/// even among units with the same properties.
/// </summary>
public record IndexedGameUnit(int Index, GameUnit Unit);


public record Game(GameRound gameRound,
    Player player1,
    Player player2,
    ImmutableList<Option<GameUnit>> world)
{
    /// <summary>
    /// The ownership of the current round.
    /// </summary>
    public Ownership Owner
    {
        get => gameRound.Value % 2 == 1
            ? this.player1.OwnerType
            : this.player2.OwnerType;
    }

    /// <summary>
    /// Determines if the game has ended by checking the castle HP of both players.
    /// Returns true if either player's castle HP is zero or less, and false otherwise.
    /// </summary>
    public bool IsEnded()
    {
        return this.player1.Castle.Hp <= HP.ZERO_HP || this.player2.Castle.Hp <= HP.ZERO_HP;
    }

    /// <summary>
    /// Executes the next round of the game. A side effect is produced in the process.
    /// It does this by incrementing the game round,
    /// and then performing castle action and unit action.
    /// The side effect is produced when the brain makes a decision on the next action.
    /// </summary>
    /// <returns>
    /// A side effect with
    /// Right: the game state of the next round
    /// Left: error if exception happened during peforming current round
    /// </returns>
    public Eff<Either<Error, Game>> NextRound()
    {
        var currentRound = this.gameRound + new GameRound(1);

        var nextGame = this with { gameRound = this.gameRound + new GameRound(1) };

        return from castleActionGameEither in nextGame.CastleAction()
               let gameEither = (
                   from castleActionGame in castleActionGameEither
                   from unitsActionGame in castleActionGame.UnitsAction()
                   select unitsActionGame)
               select gameEither;
    }

    /// <summary>
    /// Finds the player that matches the given ownership type.
    /// </summary>
    public Player GetPlayer(Ownership owner)
    {
        return player1.OwnerType == owner ? player1 : player2;
    }

    /// <summary>
    /// Updates a player's information in the game state and returns the updated state.
    /// If the player's owner type matches, their information is updated.
    /// Otherwise, their original information is kept.
    /// </summary>
    private Game UpdatePlayer(Player player)
    {
        return this with
        {
            player1 = player1.OwnerType == player.OwnerType ? player : player1,
            player2 = player2.OwnerType == player.OwnerType ? player : player2,
        };
    }

    /// <summary>
    /// Gets the spawn position of a game unit based on the player's ownership.
    /// Player 1's game unit spawns at the leftmost of the world,
    /// while player 2's game unit spawns at the rightmost of the world.
    /// </summary>
    private int GetSpawnPosition(Ownership owner)
    {
        return owner switch
        {
            Ownership.Player1 => 0,
            Ownership.Player2 => world.Length() - 1,
            _ => throw ExhaustiveMatch.Failed(owner)
        };
    }

    /// <summary>
    /// Performs a castle action and returns a side effect that is a result of
    /// the brain's decision on what action to take next.
    /// The castle first generates resources.
    /// Then, it considers the current game world and resources the castle has,
    /// and performs the action accordingly, resulting in a new game state.
    /// </summary>
    /// <returns>
    /// A side effect with
    /// Right: the game state after the castle's action
    /// Left: error if an exception happened during the castle's action
    /// </returns>
    private Eff<Either<Error, Game>> CastleAction()
    {
        var playerOriginal = GetPlayer(Owner);
        var playerAfterRoundStart = playerOriginal with { Castle = playerOriginal.Castle.GenerateResource() };
        var gameAfterStart = UpdatePlayer(playerAfterRoundStart);

        return from brainActionOption in playerAfterRoundStart.Brain.NextAction(gameAfterStart)
               let possibleGame = (
                   from brainAction in brainActionOption
                   select gameAfterStart.CastleAction(brainAction)
                )
               select possibleGame.IfNone(() => gameAfterStart);
    }

    /// <summary>
    /// Performs a castle action based on the decision of the brain.
    /// It takes in a BrainAction as a parameter and
    /// returns the game state after the castle's action.
    /// The action consists of two steps:
    /// preparing the castle by gathering the necessary resources for spawning the unit,
    /// and then spawning the unit in the game world.
    /// </summary>
    /// <returns>
    /// Right: the game state after the the castle's action
    /// Left: error if exception happened during the castle's action
    /// </returns>
    private Either<Error, Game> CastleAction(BrainAction brainAction)
    {
        return
            from prepare in this.CastleSpawnPrepareAction(brainAction)
            from x in prepare.Game.SpawnToWorld(prepare.Unit)
            select x;
    }

    /// <summary>
    /// Prepares the spawned game unit and updates the amount of gold that player has remaining.
    /// It takes in a BrainAction as a parameter and returns
    /// the spawned game unit and the game state of the updated players.
    /// The preparation consists of two steps:
    /// preparing the unit to be spawned based on the brain's action,
    /// and then deducting the necessary gold from the player's account.
    /// </summary>
    /// <param name="nextAction">preparation based on brain's action</param>
    /// <returns>
    /// Right: the spawned game unit and the game state of updated players
    /// Left: error if exception happened during the preparation
    /// </returns>
    private Either<Error, SpawnPreparedResource> CastleSpawnPrepareAction(BrainAction nextAction)
    {
        var spawnResource = PrepareSpawnUnit(nextAction, Owner);
        return GetPlayer(Owner).UseGold(spawnResource.Cost)
            .Map(x => new SpawnPreparedResource(
                spawnResource.Unit,
                this.UpdatePlayer(x)
            ));
    }

    /// <summary>
    /// Spawns a game unit to the world.
    /// If the expected position already has another game unit,
    /// the existing one is pushed forward one unit until there is an empty space.
    /// </summary>
    /// <param name="gameUnit">game unit will be spawned to the world</param>
    /// <param name="expectedIndex">the index that the game unit expected to swap</param>
    /// <returns>
    /// Right: a game state with the updated world
    /// Left: error if there is no empty space to spawn
    /// </returns>
    private Either<Error, Game> SpawnToWorld(GameUnit gameUnit)
    {
        var directionalStep = gameUnit.Owner.DirectionalStep();
        var expectedIndex = GetSpawnPosition(Owner);
        for (var index = expectedIndex; index >= 0 && index < this.world.Count; index += directionalStep)
        {
            var world = this.world;
            if (world[index].IsSome)
            {
                continue;
            }
            for (var moveIndex = index - directionalStep;
                moveIndex >= 0 && moveIndex < world.Count;
                moveIndex -= directionalStep)
            {
                world = world.SetItem(moveIndex + directionalStep, world[moveIndex]);
                world = world.SetItem(moveIndex, None);
            }
            world = world.SetItem(expectedIndex, gameUnit);
            return this with { world = world };
        }
        return Error.New("no more place to spawn");
    }

    /// <summary>
    /// This method returns a game unit with the given ownership
    /// and the amount of gold required to spawn it, depending on the brain's action
    /// </summary>
    private static SpawnResource PrepareSpawnUnit(BrainAction nextAction, Ownership owner)
    {
        return nextAction switch
        {
            BrainAction.SpawnCleric => new SpawnResource(
                new GameUnit(GameUnitType.Cleric, new HP(3), new Damage(1), new AttackRange(1), new Heal(3),
                    new CoolDown(0), new HealRange(6), owner),
                new Gold(30)),
            BrainAction.SpawnArcher => new SpawnResource(
                new GameUnit(GameUnitType.Archer, new HP(5), new Damage(2), new AttackRange(4), new Heal(0),
                    new CoolDown(0), new HealRange(0), owner),
                new Gold(20)),
            BrainAction.SpawnWarrior => new SpawnResource(
                new GameUnit(GameUnitType.Warrior, new HP(9), new Damage(4), new AttackRange(1), new Heal(0),
                    new CoolDown(0), new HealRange(0), owner),
                new Gold(15)),
            _ => throw ExhaustiveMatch.Failed(nextAction)
        };
    }

    /// <summary>
    /// Uses a recursive loop to perform an action for each game unit in the world and return the game state.
    /// The game unit closest to the opponent will take action first.
    /// </summary>
    /// <returns>
    /// Right: the game state after all the units have taken their action
    /// Left: error if an exception occurs during the action
    /// </returns>
    private Either<Error, Game> UnitsAction()
    {
        return this.GameUnitActionLoop(GetSpawnPosition(Owner.Opposite()));
    }

    /// <summary>
    /// This method repeatedly calls itself for each game unit in the world.
    /// Depending on the current round's ownership, it should start from the opponent's spawn position
    /// and ends at the current player's spawn position.
    /// </summary>
    /// <param name="index">The position of the game unit being considered.</param>
    /// <returns>
    /// Right: the game state after the unit took the action
    /// Left: error if exception occured during the actions were taking
    /// </returns>
    private Either<Error, Game> GameUnitActionLoop(int index)
    {
        if (index >= 0 && index < this.world.Count)
        {
            var directionalStep = -Owner.DirectionalStep();
            var process =
                from gameUnit in this.world[index]
                from unitPair in gameUnit.Owner == Owner ? gameUnit : Option<GameUnit>.None
                let unitAction = this.GameUnitAction(new IndexedGameUnit(index, unitPair))
                let result = unitAction.Bind(x => x.GameUnitActionLoop(index + directionalStep))
                select result;
            return process.IfNone(() =>
                this.GameUnitActionLoop(index + directionalStep)
            );
        }
        return this;
    }

    /// <summary>
    /// Performs an action for a game unit and returns an updated game state.
    /// The method starts by cooling down the healing waiting time. 
    /// After the cool down action, the unit will attempt to perform a heal action.
    /// If the heal action is successful, it will return the updated game state.
    /// If the heal action is not successful, the unit will then attempt to perform an attack action.
    /// If the attack action is successful, it will return the updated game state.
    /// If the attack action is not successful, the unit will then attempt to perform a walk action.
    /// If the walk action is successful, it will return the updated game state.
    /// If none of the above actions are successful, the method will return the game state after the cool down action.
    /// </summary>
    /// <param name="fromUnit">The unit that is performing the action</param>
    /// <returns>
    /// Right: Game object containing the updated game state
    /// Left: Error if exception occured during the action is performing
    /// </returns>
    private Either<Error, Game> GameUnitAction(IndexedGameUnit fromUnit)
    {
        var coolDownResponse = this.CoolDownAction(fromUnit);
        var fromUnitAfterCoolDown = fromUnit with { Unit = coolDownResponse.Unit };
        var process =
            coolDownResponse.Game.HealAction(fromUnitAfterCoolDown) ||
            coolDownResponse.Game.AttackAction(fromUnitAfterCoolDown) ||
            coolDownResponse.Game.WalkAction(fromUnitAfterCoolDown);

        return process.Match(x => x.Game,
            () => coolDownResponse.Game);
    }

    /// <summary>
    /// Performs a cool down action for a specific game unit.
    /// The healing waiting time for the unit is decreased by one unit after the action.
    /// </summary>
    /// <param name="unit">The game unit for which the cool down action is performed</param>
    /// <returns>
    /// a UnitSingleActionResponse object containing the updated game unit and the updated game state.
    /// </returns>
    private UnitSingleActionResponse CoolDownAction(IndexedGameUnit fromUnit)
    {
        var newFromUnit = fromUnit.Unit with
        {
            HealCoolDown = (fromUnit.Unit.HealCoolDown - new CoolDown(1)).Max(new CoolDown(-10))
        };
        return new UnitSingleActionResponse(newFromUnit,
            this with { world = this.world.SetItem(fromUnit.Index, fromUnit.Unit) });
    }

    /// <summary>
    /// Attempts to perform a heal action for a specific game unit.
    /// If the unit does not have healing ability or is in cooling down, no action is performed.
    /// Otherwise, it heals the game unit within the healing range with the lowest HP.
    /// If there is no such unit, no action is performed.
    /// </summary>
    /// <param name="fromUnit">The game unit for which the heal action is attempted</param>
    /// <returns>
    /// Some: a UnitSingleActionResponse object containing the updated game unit and the updated game state.
    /// None: no action is performed
    /// </returns>
    private Option<UnitSingleActionResponse> HealAction(IndexedGameUnit fromUnit)
    {
        if (fromUnit.Unit.Heal <= new Heal(0) || fromUnit.Unit.HealCoolDown >= new CoolDown(0))
        {
            return None;
        }
        return this.world.Select((item, index) => item.Map(x => new IndexedGameUnit(index, x)))
            .Filter(y => y.Map(x => x.Unit.Owner == fromUnit.Unit.Owner &&
                         Math.Abs(x.Index - fromUnit.Index) <= fromUnit.Unit.HealRange.Value &&
                         x.Unit.Hp < x.Unit.Type.MaxHp())
                        .IfNone(() => false))
            .OrderBy(y => y.Map(x => x.Unit.Hp).IfNone(() => HP.MAX_HP))
            .Map(x => Some(x))
            .FirstOrDefault(None)
            .Bind(x => x.Map(healableUnit => this.HealUnit(fromUnit, healableUnit)));
    }

    /// <summary>
    /// Heals a unit by applying the healing amount to the target unit.
    /// The resulting HP of the target will not exceed its maximum HP as defined by the unit type.
    /// </summary>
    /// <param name="fromUnit">The unit performing the healing action</param>
    /// <param name="healableUnit">The targeted unit being healed</param>
    /// <returns>
    /// UnitSingleActionResponse object containing the updated game unit of the performer
    /// and the updated game state which containing the updated performer and target.
    /// </returns>
    private UnitSingleActionResponse HealUnit(IndexedGameUnit fromUnit, IndexedGameUnit healableUnit)
    {
        var fromUnitAfterHeal = fromUnit.Unit with { HealCoolDown = GameUnit.MaxHealCoolDown };
        var healableUnitAfter = healableUnit.Unit.TakeHeal(fromUnit.Unit.Heal);

        return new UnitSingleActionResponse(fromUnitAfterHeal, this with
        {
            world = this.world.SetItem(fromUnit.Index, fromUnitAfterHeal)
            .SetItem(healableUnit.Index, healableUnitAfter)
        });
    }

    /// <summary>
    /// A game unit performs an attack action on a castle.
    /// The method checks if the opponent's castle is within the attack range of the attacking unit.
    /// If so, it reduces the HP of the opponent's castle.
    /// </summary>
    /// <returns>
    /// Some: UnitSingleActionResponse object containing the updated game state.
    /// None: the opponent's castle is out of the attack range.
    /// </returns>
    private Option<UnitSingleActionResponse> AttackCastleAction(IndexedGameUnit attacker)
    {
        var playerDefender = GetPlayer(attacker.Unit.Owner.Opposite());
        return Math.Abs(GetSpawnPosition(playerDefender.OwnerType) - attacker.Index) + 1
            <= attacker.Unit.AttackRange.Value
            ? new UnitSingleActionResponse(
                attacker.Unit,
                UpdatePlayer(playerDefender with
                {
                    Castle = playerDefender.Castle.TakeDamage(attacker.Unit.Damage)
                }))
            : None;
    }

    /// <summary>
    /// Attempts to attack either a castle or a unit within range.
    /// If the defender's castle has HP less than or equal to the attackable unit, the castle will be attacked.
    /// The attack may fail if the castle is out of range.
    /// If the defender's castle has HP greater than the attackable unit or the attack was failed,
    /// the unit will be attacked instead.
    /// </summary>
    /// <returns>
    /// A UnitSingleActionResponse object with the updated game state.
    /// </returns>
    private UnitSingleActionResponse AttackCastleOrUnitInRangeAction(
        IndexedGameUnit attackerUnit, IndexedGameUnit attackableUnit)
    {
        var playerDefender = GetPlayer(attackerUnit.Unit.Owner.Opposite());
        var attackCastle = playerDefender.Castle.Hp <= attackableUnit.Unit.Hp
            ? AttackCastleAction(attackerUnit)
            : None;
        return attackCastle.IfNone(() => AttackUnit(attackerUnit, attackableUnit));
    }

    /// <summary>
    /// Attempts to perform an attack action for a specific game unit.
    /// It first attempts to attack the opponent's castle.
    /// If unsuccessful, it will then attempt to attack a unit with the lowest HP within the attack range.
    /// </summary>
    /// <returns>
    /// Some: UnitSingleActionResponse object containing the updated game unit and game state
    /// None: if the attacker lacks the attack ability or both the opponent's castle and units are out of range
    /// </returns>
    private Option<UnitSingleActionResponse> AttackAction(
        IndexedGameUnit attackerUnit)
    {
        if (attackerUnit.Unit.Damage <= new Damage(0))
        {
            return None;
        }

        return this.world.Select((item, index) => item.Map(x => new IndexedGameUnit(index, x)))
            .Filter(y => y.Map(x => x.Unit.Owner != attackerUnit.Unit.Owner
                        && Math.Abs(x.Index - attackerUnit.Index) <= attackerUnit.Unit.AttackRange.Value)
                .IfNone(() => false))
            .OrderBy(y => y.Map(x => x.Unit.Hp).IfNone(() => HP.MAX_HP))
            .FirstOrDefault(None)
            .Match(attackableUnit =>
                this.AttackCastleOrUnitInRangeAction(attackerUnit, attackableUnit),
                this.AttackCastleAction(attackerUnit)
            );
    }

    /// <summary>
    /// Performs an attack from one unit to another.
    /// The targeted unit's HP will be decreased and if it reaches zero, it will be removed from the world.
    /// </summary>
    /// <returns>
    /// A UnitSingleActionResponse object containing the attacking unit and the updated game state.
    /// </returns>
    private UnitSingleActionResponse AttackUnit(
        IndexedGameUnit fromUnit, IndexedGameUnit attackableUnit)
    {
        var attackableUnitAfter = attackableUnit.Unit.TakeDamage(fromUnit.Unit.Damage);

        var newWorld = this.world.SetItem(attackableUnit.Index,
            attackableUnitAfter.Hp > new HP(0) ? attackableUnitAfter : None);

        return new UnitSingleActionResponse(fromUnit.Unit, this with { world = newWorld });
    }

    /// <summary>
    /// This method performs a walk action for a specified unit.
    /// The unit can move one step towards the opponent's location.
    /// If the targeted position is inside the world's bounds and unoccupied, the unit will move there.
    /// If there's a friendly unit blocking the way and the unit's HP is lower,
    /// their positions will be swapped.
    /// The unit will not move if it's already next to the opponent's castle or
    /// has higher or equal HP than the friendly blocking unit.
    /// </summary>
    /// <returns>
    /// Some: UnitSingleActionResponse with updated game state and game unit.
    /// None: The game unit did not move.
    /// </returns>
    private Option<UnitSingleActionResponse> WalkAction(IndexedGameUnit fromUnit)
    {
        var directionalStep = Owner.DirectionalStep();
        var nextIndex = fromUnit.Index + directionalStep;
        if (nextIndex < 0 || nextIndex >= this.world.Count)
        {
            return None;
        }
        if (this.world[nextIndex].IsNone)
        {
            var newDirectWorld = this.world.SetItem(nextIndex, fromUnit.Unit);
            newDirectWorld = newDirectWorld.SetItem(fromUnit.Index, None);
            return new UnitSingleActionResponse(fromUnit.Unit, this with { world = newDirectWorld });
        }
        var targetUnit = this.world[nextIndex].ToSeq().First();
        if (targetUnit.Owner != fromUnit.Unit.Owner)
        {
            return None;
        }
        if (targetUnit.Hp >= fromUnit.Unit.Hp)
        {
            return None;
        }
        var newWorld = this.world.SetItem(nextIndex, fromUnit.Unit);
        newWorld = newWorld.SetItem(fromUnit.Index, targetUnit);
        return new UnitSingleActionResponse(fromUnit.Unit, this with { world = newWorld });
    }
}
